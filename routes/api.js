var express = require('express');
var urlParser = require('url');
var router = express.Router();
var OAuth = require('oauth');

/* GET users listing. */
router.get('/', function(req, res, next) {
    let authTokens;

    try {
        authTokens = require('../oauth.conf.json');
    } catch(e) {
        const fileRequiredMsg = 'oauth.conf.json file required to use this endpoint';
        console.error(fileRequiredMsg);
        res.status(500);
        return res.json({ error: fileRequiredMsg });
    }

    let oauth = new OAuth.OAuth(
        'https://api.twitter.com/oauth/request_token',
        'https://api.twitter.com/oauth/access_token',
        authTokens.twitterOauth.consumer.key,
        authTokens.twitterOauth.consumer.secret,

        '1.0A',
        null,
        'HMAC-SHA1'
    );

    oauth.get(
        'https://api.twitter.com/1.1/search/tweets.json?' + urlParser.parse(req.url).query,
        authTokens.twitterOauth.user.token,
        authTokens.twitterOauth.user.secret,
        handleResponse
    );

    function handleResponse(e, data) {
        if (e) {
            res.json({
                errorMsg: 'There was an auth error',
                error: e
            });
        } else {
            res.json(JSON.parse(data));
        }
    }
});

module.exports = router;
